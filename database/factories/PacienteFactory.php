<?php

namespace Database\Factories;

use App\Models\Paciente;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class PacienteFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Paciente::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'nombre' => $nombre = $this->faker->name,
            'slug'=> Str::slug($nombre),
            'vacunado' => $prueba = $this->faker->boolean,
            'grupo_id' => $this->faker->numberBetween(1,7),
            'fechaVacuna' => $this->faker->dateTimeThisYear

        ];
    }
}
