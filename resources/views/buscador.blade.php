
@extends('master')
@section('titulo')
Buscador
@endsection
@section('contenido')

<input id="busqueda" type="text" placeholder="Introduce el nombre del paciente">

<script>
    $(document).ready(function () {
        $("#busqueda").autocomplete({
            source: function( request, response ) {
                $.ajax( {
                    type: "POST",
                    url: "{{url('pacientes/buscar')}}",
                    dataType: "json",
                    data: {
                        "_token": "{{csrf_token()}}",
                        "busqueda": request.term
                    },
                    success: function( data ) {
                        response( data );
                    }
                } );
            },
            select: function (event, ui) {
                window.location = window.location.origin + "/pacientes/" + convertToSlug(ui.item.value);
            }
        } );
        function convertToSlug(Text)
        {
            return Text
                .toLowerCase()
                .replace(/ /g,'-')
                .replace(/[^\w-]+/g,'');
        }
    });
</script>
@endsection






