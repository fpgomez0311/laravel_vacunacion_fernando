<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\Paciente;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::table('pacientes')->delete();
        $this->call(PacienteSeeder::class);
        \App\Models\Paciente::factory(25)->create();

        DB::table('grupos')->delete();
        $this->call(GrupoSeeder::class);
    
        DB::table('vacunas')->delete();
        $this->call(VacunaSeeder::class);
    }
}
