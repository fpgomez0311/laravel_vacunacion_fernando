<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Paciente;

class PacienteController extends Controller
{
    public function buscar(Request $request)
    {
        $busqueda = $request->busqueda;
        $pacientes = Paciente::select('nombre')
            ->where('nombre', 'like', '%$busqueda%')
            ->pluck('nombre');
        return response()->json($pacientes);
        
    }
    public function buscador(){
        return view('buscador');

    }
}
