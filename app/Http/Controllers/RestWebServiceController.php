<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Vacuna;
use App\Models\Paciente;


class RestWebServiceController extends Controller
{
    public function create(Request $request){
        $vacuna = new Vacuna();
        $vacuna->nombre = $request->nombre;
        $vacuna->save();
        return response()->json(['mensaje'=>'Vacuna: '.$vacuna->nombre. ' ha sido insertada correctamente.']);
    }
    public function vacunas($idPaciente){
        $grupo = Paciente::findOrFail($idPaciente);
        $vacunas = Vacuna::findOrFail($grupo->grupo_id);
        return response()->json($vacunas);
    }
}
