<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGrupoVacunaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('grupo_vacuna', function (Blueprint $table) {
            $table->id('grupo_id')->unsigned();
            $table->id('vacuna_id')->unsigned();
        
            $table->foreign('grupo_id')->references('id')->on('grupos');
            $table->foreign('vacuna_id')->references('id')->on('vacunas');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('grupo_vacuna');
    }
}
